<?php

namespace furikuri\behavior\upload\models;

use Yii;

/**
 *
 *
 * Interface UploadsInterface
 * @package furikuri\behavior\upload\models
 */
interface UploadsInterface
{
    public function getName();
    public function getExtension();
}
